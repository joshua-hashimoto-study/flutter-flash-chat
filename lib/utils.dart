import 'package:flutter/material.dart';

unfocusKeyword(BuildContext context) {
  FocusScopeNode currentFocus = FocusScope.of(context);
  if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus(); // 入力欄のフォーカスを外す。
  }
}
