import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

const borderRadius = Radius.circular(30.0);

class MessageBubble extends StatelessWidget {
  MessageBubble({this.use, this.isCurrentUser});

  final DocumentSnapshot use;
  final bool isCurrentUser;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: isCurrentUser ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            '${use.data["sender"]}',
            style: TextStyle(
              fontSize: 12.0,
              color: Colors.black54,
            ),
          ),
          Material(
            borderRadius: isCurrentUser
                ? BorderRadius.only(
                    topLeft: borderRadius,
                    bottomLeft: borderRadius,
                    bottomRight: borderRadius,
                  )
                : BorderRadius.only(
                    topRight: borderRadius,
                    bottomLeft: borderRadius,
                    bottomRight: borderRadius,
                  ),
            elevation: 5.0,
            color: isCurrentUser ? Colors.lightBlueAccent : Colors.white,
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: 10.0,
                horizontal: 20.0,
              ),
              child: Text(
                '${use.data["text"]}',
                style: TextStyle(
                  fontSize: 15.0,
                  color: isCurrentUser ? Colors.white : Colors.black54,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
